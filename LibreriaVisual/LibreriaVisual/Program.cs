﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Libreria;

namespace LibreriaVisual
{
    class LibreriaVisual
    {
        static void Main(string[] args)
        {
            int opcion;
            do
            {
                System.Console.WriteLine("elige una opcion");
                System.Console.WriteLine();
                System.Console.WriteLine("1 - mostrar pares del uno al 100");
                System.Console.WriteLine("2 - mostrar impares del uno al 100");
                System.Console.WriteLine("3 - escribe frases palabra a palabra");
                System.Console.WriteLine("4 - pide y muestra 10 enteros");
                System.Console.WriteLine("5 - salir");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        LibreriaVS.muestraPares();
                        break;
                    case 2:
                        LibreriaVS.muestraImpares();
                        break;
                    case 3:
                        LibreriaVS.creaFrase();
                        break;
                    case 4:
                        LibreriaVS.pideMuestraEnteros();
                        break;
                    case 5:
                        System.Console.WriteLine("saliste");
                        break;
                    default:
                        System.Console.WriteLine("selecciona una opcion correcta");
                        break;
                }
            } while (opcion != 5);
        }
    }
    
}
