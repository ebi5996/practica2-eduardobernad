package libreriaEclipse;
import librerias.Libreria;
import java.util.Scanner;

public class LibreriaEclipse {

	public static void main(String[] args) {
		int opcion;
		Scanner input = new Scanner(System.in);
		do {
			System.out.println("elige una opcion");
			System.out.println();
			System.out.println("1 - mostrar pares del uno al 100");
			System.out.println("2 - mostrar impares del uno al 100");
			System.out.println("3 - escribe frases palabra a palabra");
			System.out.println("4 - pide y muestra 10 enteros");
			System.out.println("5 - salir");
			opcion = input.nextInt();
			input.nextLine();
			switch(opcion) {
			case 1:
				Libreria.muestraPares();
				break;
			case 2:
				Libreria.muestraImpares();
				break;
			case 3:
				Libreria.creaFrase(input);
				break;
			case 4:
				Libreria.pideMuestraEnteros( input);
				break;
			case 5:
				System.out.println("saliste");
				break;
			default:
				System.out.println("selecciona una opcion correcta");
				break;
			}
		}while(opcion != 5);
	}

}
